Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: checker-framework
Source: https://github.com/typetools/checker-framework
Files-Excluded: gradlew*
                gradle
                docs/tutorial/*
                checker/jdk/*
                docs/developer/release/*
 				.github/*
 				checker/jtreg/nullness/issue2173/*
 				dataflow/manual/*
 				checker/src/testannotations/java/javax/validation/constraints/NotNull.java
Comment: Exclude Gradle wrapper, embedded JAR files, embedded JDK, upstream
 release files, embedded class/sty files, and a file with a problematic license.
 .
 Files using the MIT license have been updated based on upstream clarification
 at https://github.com/typetools/checker-framework/issues/3429. This is
 implemented upstream after version 3.5.0. Once such a version is packaged in
 Debian, this clarification paragraph can be removed.
 .
 Upstream has also clarified that various test annotations are clean-room
 reimplementations of upstream specifications as detailed in
 https://github.com/typetools/checker-framework/issues/3819. This is
 implemented upstream after version 3.7.0. Once such a version is packaged in
 Debian, this clarification paragraph can be removed.
 .
 Full list of project contributors is located at docs/manual/contributors.tex.

Files: *
Copyright: 2004-2020 Checker Framework developers
           2004-2020 Michael Ernst <mernst@cs.washington.edu>
           2004-2020 Werner M. Dietl <wdietl@uwaterloo.ca>
           2004-2020 Suzanne Millstein <smillst@cs.washington.edu>
License: GPL-2.0-with-classpath-exception

Files: **/*.astub
       **/qual/*.java
       **/FormatUtil.java
       **/I18nFormatUtil.java
       **/NullnessUtil.java
       **/Opt.java
       **/PurityUnqualified.java
       **/RegexUtil.java
       **/SignednessUtil.java
       **/SignednessUtilExtra.java
       **/UnitsTools.java
       checker/src/testannotations/**/*.java
       framework/src/main/java/org/jmlspecs/**/*.java
Copyright: 2004-2020 Checker Framework developers
           2004-2020 Michael Ernst <mernst@cs.washington.edu>
           2004-2020 Werner M. Dietl <wdietl@uwaterloo.ca>
           2004-2020 Suzanne Millstein <smillst@cs.washington.edu>
License: MIT

Files: debian/*
Copyright: 2020-2023 Olek Wojnar <olek@debian.org>
License: MIT

License: GPL-2.0-with-classpath-exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of version 2 of the GNU General Public License as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file `/usr/share/common-licenses/GPL-2'.
 .
 .
 "CLASSPATH" EXCEPTION TO THE GPL
 .
 Certain source files distributed by Oracle America and/or its affiliates are
 subject to the following clarification and special exception to the GPL, but
 only where Oracle has expressly included in the particular source file's header
 the words "Oracle designates this particular file as subject to the "Classpath"
 exception as provided by Oracle in the LICENSE file that accompanied this code."
 .
    Linking this library statically or dynamically with other modules is making
    a combined work based on this library.  Thus, the terms and conditions of
    the GNU General Public License cover the whole combination.
 .
    As a special exception, the copyright holders of this library give you
    permission to link this library with independent modules to produce an
    executable, regardless of the license terms of these independent modules,
    and to copy and distribute the resulting executable under terms of your
    choice, provided that you also meet, for each linked independent module,
    the terms and conditions of the license of that module.  An independent
    module is a module which is not derived from or based on this library.  If
    you modify this library, you may extend this exception to your version of
    the library, but you are not obligated to do so.  If you do not wish to do
    so, delete this exception statement from your version.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
